﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Snake
{
    public class MainSnake
    {
        public static int n=0;
        public static int pause = 0;
        public static int level = 0;
        public static int sleep = 1000;
        public static int helpX = 0;
        public static int helpY = 0;

        public static void Main()
        {
            Console.WindowHeight = 25;
            Console.WindowWidth = 81;
            Console.BufferHeight = 25;
            Console.BufferHeight = 81;
            WallLeftAndRight();
            WallUpAndDown();
            Picture();


            try
            {
                TheEnd();
            }
            catch (FormatException)
            {

                Console.CursorVisible = false;
                level = 90;
                Game(level);
            }


        }

        public static void Game(int level)
        {
            int x = 10;
            int y = 10;
            List<Snake> snake = new List<Snake>();
            snake.Add(new Snake(x, y,1,0));
            snake.Add(new Snake(snake[0].x - 1, snake[0].y,1,0));
            snake.Add(new Snake(snake[0].x - 2, snake[0].y,1,0));
            snake.Add(new Snake(snake[0].x - 3, snake[0].y, 1, 0));
            Random rnd = new Random();
            int xBug = rnd.Next(4, 75);
            int yBug = rnd.Next(4, 20);
            Bug bug = new Bug(xBug, yBug);
            Click(snake, bug);          
        }

        public static void Click(List<Snake> snake, Bug bug)
        {
            int keyNow = 1;
            int keyNot = 0;
            Console.Clear();
            WallUpAndDown();
                WallLeftAndRight();

            while (true)
            {
                //Thread.Sleep(sleep - (10 * level));
                bug.Show();
                KillsCount(MainSnake.n);

                if (Console.KeyAvailable == true)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.DownArrow:
                            
                            keyNot = keyNow;
                            
                            if (keyNot != 3)
                            {
                                keyNow = 4;
                                snake[0].dx = 0;
                                snake[0].dy = 1;
                                run(snake, bug);
                                break;
                            }
                            else
                                break;

                        case ConsoleKey.UpArrow:
                            
                            keyNot = keyNow;
                            
                            if (keyNot != 4)
                            {
                                keyNow = 3;
                                snake[0].dx = 0;
                                snake[0].dy = -1;
                                run(snake, bug);
                                break;
                            }
                            else
                                break;

                        case ConsoleKey.LeftArrow:
                            
                            keyNot = keyNow;
                            
                            if (keyNot != 2)
                            {
                                keyNow = 1;
                                snake[0].dx = -1;
                                snake[0].dy = 0;
                                run(snake, bug);
                                break;
                            }
                            else
                                break;

                        case ConsoleKey.RightArrow:
                            
                            keyNot = keyNow;
                            
                            if (keyNot != 1)
                            {
                                keyNow = 2;
                                snake[0].dx = 1;
                                snake[0].dy = 0;
                                run(snake, bug);
                                break;
                            }
                            else
                                break;
   
                        case ConsoleKey.Backspace:
                                helpX = snake[0].dx;
                                helpY = snake[0].dy;
                            Pause();
                            Console.Clear();
                            WallLeftAndRight();
                            WallUpAndDown();
                            KillsCount(n);
                            break;

                        case ConsoleKey.Spacebar:

                            break;

                        default:

                            break;
                    }
                }
                else
                {
                    run(snake, bug);
                }
               Thread.Sleep(sleep - (10 * level));
                
            }
        }

        public static void run(List<Snake> snake, Bug bug)
        {


            snake[0].x += snake[0].dx;
                snake[0].y += snake[0].dy;
                snake[0].Draw();


            if (snake[0].x==bug.xBug && snake[0].y == bug.yBug)
            {
                MainSnake.n++;
                Random rnd = new Random();
                bug.xBug=rnd.Next(4, 75);
                bug.yBug = rnd.Next(4, 20);
                snake.Add(new Snake(snake[snake.Count - 1].x - snake[snake.Count - 1].dx, snake[snake.Count - 1].y - snake[snake.Count - 1].dy, snake[snake.Count - 1].dx, snake[snake.Count - 1].dy));
            }


                for (int i = snake.Count - 1; i > 0; i--)
                {
                Console.SetCursorPosition(snake[snake.Count - 1].x, snake[snake.Count - 1].y);
                Console.WriteLine(' ');
                snake[i].x += snake[i].dx;
                    snake[i].y += snake[i].dy;
                    snake[i].dx = snake[i - 1].dx;
                    snake[i].dy = snake[i - 1].dy;
                Console.SetCursorPosition(snake[snake.Count - 1].x, snake[snake.Count - 1].y);
                Console.WriteLine(' ');
                
                Console.CursorVisible = false;
                
                snake[i].Draw();
                if (snake[0].x==snake[i].x && snake[0].y == snake[i].y)
                {
                    for (int j = 0; j < snake.Count; j++)
                    {
                        snake[j].dx = 0;
                        snake[j].dy = 0;
                    }
                    TheEnd();
                }
            }
                if (snake[0].x == 2 || snake[0].x == 77 || snake[0].y == 2 || snake[0].y == 22)
            {
                for (int j = 0; j < snake.Count; j++)
                {
                    snake[j].dx = 0;
                    snake[j].dy = 0;
                }
                TheEnd();
            }



        }

        public static void KillsCount(int n)
        {
            Console.SetCursorPosition(2, 1);
            Console.WriteLine("Количество очков: " + n);
        }

        public static void WallUpAndDown()
        {
            for (int i=2; i<79; i+=2){
                Console.SetCursorPosition(i, 2);
                Console.WriteLine('☼');
                Console.SetCursorPosition(i, 22);
                Console.WriteLine('☼');
            }
        }
          public static void WallLeftAndRight()
        {
            for (int i = 3; i < 23; i++)
            {
                Console.SetCursorPosition(2, i);
                Console.WriteLine('☼');
                Console.SetCursorPosition(78, i);
                Console.WriteLine('☼');
            }
        }
        public static void TheEnd()
        {
            Console.Beep(440, 200);
            Console.SetCursorPosition(24, 17);
            Console.WriteLine("Нажмите Enter, чтобы начать игру,");
            Console.SetCursorPosition(22, 18);
            Console.WriteLine("или Escape - для выхода из программы.");
            Console.CursorVisible = false;
            while (true) {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Enter:
                        Console.SetCursorPosition(23, 19);
                        Console.WriteLine("Введите уровень сложности (1-99).");
                        Console.SetCursorPosition(23, 20);
                        Console.CursorVisible = true;
                        level = Convert.ToInt32(Console.ReadLine());
                        n = 0;
                        Game(level);
                        break;

                    case ConsoleKey.Escape:
                        Environment.Exit(0);
                        break;

                    default:

                        break;
                }
            }
        }

        public static void Pause()
        {
            Console.Beep(440, 200);
            Console.SetCursorPosition(21, 17);
            Console.WriteLine("Нажмите Spacebar, чтобы продолжить игру, ");
            Console.SetCursorPosition(22, 18);
            Console.WriteLine("или Escape - для выхода из программы.");
            Console.CursorVisible = false;
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.Enter:
                    Game(level);
                    break;

                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;
            }
        }

        public static void Picture()
        {
            Console.SetCursorPosition(25, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(26, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(27, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(24, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(28, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(24, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(25, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(26, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(27, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(28, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(24, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(28, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(25, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(26, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(27, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(32, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(33, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(34, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(31, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(35, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(40, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(39, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(41, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(42, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(42, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(39, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(40, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(41, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(42, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(42, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(38, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(42, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(49, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(48, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(47, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(46, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(47, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(48, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(45, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(49, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(53, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(54, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(55, 7);
            Console.WriteLine("█");
            Console.SetCursorPosition(52, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(56, 8);
            Console.WriteLine("█");
            Console.SetCursorPosition(52, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(56, 9);
            Console.WriteLine("█");
            Console.SetCursorPosition(52, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(53, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(54, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(55, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(56, 10);
            Console.WriteLine("█");
            Console.SetCursorPosition(52, 11);
            Console.WriteLine("█");
            Console.SetCursorPosition(52, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(56, 12);
            Console.WriteLine("█");
            Console.SetCursorPosition(53, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(54, 13);
            Console.WriteLine("█");
            Console.SetCursorPosition(55, 13);
            Console.WriteLine("█");
        }

    }
}
