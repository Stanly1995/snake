﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public class Bug
    {
        public int xBug;
        public int yBug;

        public Bug(int xBug, int yBug)
        {
            this.xBug = xBug;
            this.yBug = yBug;
        }

        public void Show()
        {
            Console.CursorVisible = false;
            Console.SetCursorPosition(xBug, yBug);
            Console.Write('♥');
        }
    }
}
